from delune import _delune

class xml_handler:
	def __init__ (self):
		self.ignore_data = 0
		self.maybe_data = 0
		self.anchors = []
		self.text = ""
		self.temp = ""
		
	def finish_starttag(self, tag, attrs):
		if tag in ('style', 'script', 'object'):
			self.ignore_data = 1	
		print("#start tag:", tag, attrs)
		
	def finish_endtag(self,tag):
		if tag in ('style', 'script', 'object'):
			if self.temp.find ("/*") > -1 and self.temp.find ("*/") == -1:
				self.maybe_data = 1
				return
			if self.temp.find ("//") > -1 and self.temp.find ("\n") == -1:
				self.maybe_data = 1
				return				
			else:
				self.ignore_data = 0
				self.maybe_data = 0
				self.text += self.temp
				self.temp = ""
								
		print("#end tag:", tag)
		
	def handle_data(self,data):
		if self.ignore_data:
			return
			
		if self.maybe_data:
			self.temp += data
		
		else:
			print("#data: ", repr(data.strip ()))
			self.text += data
		
	def handle_comment(self, text):
		print("-comment: ", text)
		
	def resolve_entityref(self, ref):
		print("#ref: ", ref)
		return "&"
		

parser = _delune.XMLParser()

target = xml_handler()
parser.register(target)

file = open ("g:/ttt.htm")

for i in range (100000):
	data = file.read()
	parser.feed(data)
	print(".", end=' ')
	#print "-" * 79
	#print `target.text [:200]`
	
parser.close()






