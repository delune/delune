import os
import pytest
import os
import csv
import shutil
import delune
from delune.fields import *
from rs4 import pathtool
          
def test_index (naics, rdir, _index):
    confdir = os.path.join (rdir, "delune", "config")
    coldir = os.path.join (rdir, "delune", "collections")
    
    dl = delune.mount (rdir)
    assert os.path.isdir (confdir)
    assert os.path.isdir (coldir)
    
    col = dl.create ("testcol", ["testcol"])
    col.setopt ("analyzer", make_lower_case = True, stem_level = 1)
    assert os.path.isfile (os.path.join (confdir, "testcol"))
    
    _index (col, naics)
    col.rollback ()
       
    _index (col, naics, 500)
    col.commit ()
    assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) == 6
    dl.index ()
    assert len (os.listdir (os.path.join (coldir, "testcol", ".que"))) == 0
    col.close ()
    
    col = dl.load ("testcol")
    with col.documents as ds:
        r = ds.search ("service")
        assert "id" in r ["result"][0][0]
        assert r ['total'] == 225
        
        r = ds.search ("service", nth_content = 1)        
        assert "full" in r ["result"][0][0]
        assert "<b>" in r ["result"][0][1]
        
        r = ds.search ("service",  partial = "code")
        assert r ["code"] == 500
        
        r = ds.search ("service",  partial = "id")
        assert len (r ["result"][0][0]) == 1
        assert "<b>" in r ["result"][0][1]
    
    _index (col, naics, 500)
    _index (col, naics, 500)
    
    col.commit ()
    dl.index ()
    
    with col.documents as ds:
        r = ds.search ("service")        
        assert r ['total'] == 225        
        
        ds.qdelete ("service")        
        col.commit ()
        dl.index ()
        
        r = ds.search ("service")        
        assert r ['total'] == 0
        
        r = ds.search ("equipment")        
        assert r ['total'] == 111
        
        ds.truncate ("testcol")
        col.commit ()
        dl.index ()
        
        r = ds.search ("equipment")        
        assert r ['total'] == 0
        
        r = ds.search ("service")        
        assert r ['total'] == 0
        
    _index (col, naics, 500)
    col.commit ()
    dl.index ()
    
    with col.documents as ds:    
        r = ds.search ("service")
        assert r ['total'] == 225
        
        r = ds.get ("812320")        
        assert r ['total'] == 1
                        
        ds.delete ("812320")
        col.commit ()
        dl.index ()        
        r = ds.get ("812320")        
        assert r ['total'] == 0                
        
    _index (col, naics, 500, False)
    col.commit ()
    dl.index ()
    
    with col.documents as ds:        
        r = ds.search ("service")
        assert r ['total'] == 449
        
        ds.qdelete ("service")
        col.commit ()
        dl.index ()
        
        r = ds.search ("service")
        assert r ['total'] == 0
        
    
    col.close ()
    col.drop (True)
    assert not os.path.isdir (os.path.join (confdir, "testcol"))
    
    