# coding=utf8

from delune import standard_analyzer
import time
import rs4 

def test_animals (animals):
    a = standard_analyzer (10000)        
    a.setopt (ngram = 0, stem_level = 0)
    y = a.index (animals)
    assert len (y) == 11
    assert y ["snake"] == [10, 11]
    
    a.setopt (make_lower_case = True)
    y = a.index (animals)
    assert len (y) == 7
    assert y ["cats"] == [4, 5]
    assert y ["snake"] == [9, 10, 11]
    
    a.setopt (stem_level = 1)
    y = a.index (animals)
    assert len (y) == 4
    assert set (y.keys ()) == {"lion", "snake","dog","cat"}
    assert sum ([len (v) for v in y.values ()]) == 12
    
    a.setopt (strip_html = 1)
    y = a.index (animals)
    assert len (y) == 4
    assert set (y.keys ()) == {"lion", "snake","dog","cat"}
    assert sum ([len (v) for v in y.values ()]) == 10
    assert (y ["snake"] == [9])
    
    a.setopt (stem_level = 1, make_lower_case = False)
    y = a.index (animals)    
    assert y ["CATS"] == [5]
    a.close ()
    
def test_sentence (sentence):
    a = standard_analyzer (10000)        
    a.setopt (stem_level = 1, make_lower_case = True)
    y = a.index (sentence)
    assert "1914" in y
    a.setopt (contains_alpha_only = True)
    y = a.index (sentence)
    assert "1914" not in y
    a.close ()

def test_speed ():
    d = """    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
        which the terms are word pairs. Another way to support
    phrase based query modes is to index and store phrases
    directly [8] or simply by using an inverted index and ap-
    proximating phrases through a ranked query technique [5,
    10]. Greater eciency, with no additional in-memory space
    overheads, is possible with a special-purpose structure, the
    nextword index [15], where search structures are used to ac-
    celerate processing of word pairs. The nextword index takes
    the middle ground by indexing pairs of words and, therefore,
    is particularly good at resolving phrase queries containing
    two or more words. As noted above and observed elsewhere,
    the commonest number of words in a phrase is two [14].
    A nextword index is a three-level structure. The highest
    level is of the distinct index terms in the collection, which we
    call rstwords. At the middle level, for each rstword there
    is a data structure (such as a front-coded list, or for fast
            which the terms are word pairs. Another way to support
    phrase based query modes is to index and store phrases
    directly [8] or simply by using an inverted index and ap-
    proximating phrases through a ranked query technique [5,
    10]. Greater eciency, with no additional in-memory space
    overheads, is possible with a special-purpose structure, the
    nextword index [15], where search structures are used to ac-
    celerate processing of word pairs. The nextword index takes
    the middle ground by indexing pairs of words and, therefore,
    is particularly good at resolving phrase queries containing
    two or more words. As noted above and observed elsewhere,
    the commonest number of words in a phrase is two [14].
    A nextword index is a three-level structure. The highest
    level is of the distinct index terms in the collection, which we
    call rstwords. At the middle level, for each rstword there
    is a data structure (such as a front-coded list, or for fast
    finalle    
    """
    s = time.time ()
    a = standard_analyzer (10000)        
    a.setopt (stem_level = 1, make_lower_case = True)
    for i in range (10000):
        a.index (d)
    print (time.time () - s)
    
    a = standard_analyzer (10000, numthread = 8)    
    a.setopt (stem_level = 1, make_lower_case = True)
    assert len (a.index (d)) == 67
    
    s = time.time ()
    r = 1000
    with rs4.threading (8) as exe:
        fs = [exe.submit(a.index, d) for n in range(1000)]
        done, _ = rs4.waitf (fs)
        assert len (done) == 1000
        result = sum ([len (f.result()) for f in done])
        assert result == 67 * 1000
    print (time.time () - s)    
    a.close ()
    
    a = standard_analyzer (10000, numthread = 1)    
    a.setopt (stem_level = 1, make_lower_case = True)
   
   